# [apt-packages-demo.gitlab.io](https://apt-packages-demo.gitlab.io/)

## Popular packages
* [packages.debian.org](https://www.debian.org/distrib/packages)/[sid](https://packages.debian.org/sid/):
  @ [Google](https://www.google.com/search?q=site%3Apackages.debian.org)

## Some packages
* [task-spooler](https://tracker.debian.org/pkg/task-spooler)